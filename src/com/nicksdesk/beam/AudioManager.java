package com.nicksdesk.beam;

import java.util.HashMap;
import java.util.Map;

import org.lwjgl.openal.AL;
import org.newdawn.slick.Music;
import org.newdawn.slick.Sound;

public class AudioManager {

	public static Map<String, Sound> soundMap = new HashMap<String, Sound>();
	public static Map<String, Music> musicMap = new HashMap<String, Music>();
	
	public static void init() throws Exception {
		//musicMap.put("background", new Music("assets/background.ogg"));
		soundMap.put("click", new Sound("assets/click.ogg"));
	}
	
	//TODO: fix audio cleanup
	public static void cleanUp() {
		if(AL.isCreated()) {
			soundMap.clear();
			musicMap.clear();
			AL.destroy();
		}
	}
	
	public static Music getMusic(String name) {
		return musicMap.get(name);
	}
	
	public static Sound getSound(String name) {
		return soundMap.get(name);
	}
	
	public static void addMusic(String name, String path) {
		try {
			musicMap.put(name, new Music(path));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void addSound(String name, String path) {
		try {
			soundMap.put(name, new Sound(path));
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
