package com.nicksdesk.beam;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class TrackingEnemy extends GameObject {

	private Handler handler;
	private GameObject player;
	
	private final int diffConst = 8;
	
	public TrackingEnemy(int x, int y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		for(int i = 0; i < handler.object.size(); i++) {
			if(handler.object.get(i).getId() == ID.Player) player = handler.object.get(i);
		}
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, 16, 16);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		float diffX = x - player.getX() - diffConst;
		float diffY = y - player.getY() - diffConst;
		float dist = (float) Math.sqrt((x - player.getX()) * (x - player.getX()) + (y - player.getY()) * (y - player.getY()));
		
		velX = (float) ((-1.0 / dist) * diffX);
		velY = (float) ((-1.0 / dist) * diffY);
		
		if(y <= 0 || y >= Game.HEIGHT - 32) {
			velY *= -1;
		}
		if(x <= 0 || x >= Game.WIDTH - 16) {
			velX *= -1;
		}
		
		handler.addObject(new Trail((int)x, (int)y, Color.YELLOW, 16, 16, 0.02f, handler, ID.Trail));
		
	}

	public void render(Graphics g) {
		g.setColor(Color.YELLOW);
		g.fillRect((int)x, (int)y, 16, 16);
	}
}