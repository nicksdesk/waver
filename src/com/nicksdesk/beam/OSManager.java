package com.nicksdesk.beam;

import java.io.File;

public class OSManager {
	
	public static void setNatives() {
		
		if(getOS().contains("win")) {
			System.setProperty("org.lwjgl.librarypath", new File("assets/natives/windows").getAbsolutePath());
		} else if(getOS().contains("mac")) {
			System.setProperty("org.lwjgl.librarypath", new File("assets/natives/macosx").getAbsolutePath());
		} else if(getOS().contains("linux")) {
			System.setProperty("org.lwjgl.librarypath", new File("assets/natives/linux").getAbsolutePath());
		} else if(getOS().contains("solaris")) {
			System.setProperty("org.lwjgl.librarypath", new File("assets/natives/solaris").getAbsolutePath());
		}
		
		
	}
	
	private static String getOS() {
		return System.getProperty("os.name").toLowerCase();
	}
	
}
