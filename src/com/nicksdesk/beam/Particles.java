package com.nicksdesk.beam;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class Particles extends GameObject {

	private Handler handler;
	private Random r = new Random();  
	
	private int randRed = r.nextInt(255);
	private int randGreen = r.nextInt(255);
	private int randBlue = r.nextInt(255);
	
	private Color color;
	
	public Particles(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		
		velX = (r.nextInt(6 - (-6)) + (-6));
		velY = (r.nextInt(6 - (-6)) + (-6));
		
		if(velX == 0) velX = 1;
		if(velY == 0) velY = 1;
		
		color = new Color(randRed, randGreen, randBlue);
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, 16, 16);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		if(y <= 0 || y >= Game.HEIGHT - 32) {
			velY *= -1;
		}
		if(x <= 0 || x >= Game.WIDTH - 16) {
			velX *= -1;
		}
		
		handler.addObject(new Trail((int)x, (int)y, color, 16, 16, 0.02f, handler, ID.Trail));
		
	}

	public void render(Graphics g) {
		g.setColor(color);
		g.fillRect((int)x, (int)y, 16, 16);
	}
}