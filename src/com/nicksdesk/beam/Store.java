package com.nicksdesk.beam;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JOptionPane;

import com.nicksdesk.beam.Game.STATE;

public class Store extends MouseAdapter {

	private Handler handler;
	private HUD hud;

	private Font title = new Font("arial", 0, 60);
	private Font button = new Font("arial", 1, 20);
	private Font price = new Font("arial", 0, 14);
	
	private int refillHealthCost = 4500;
	private int healthCost = 2500;
	private int speedCost = 1000;
	
	public Store(Handler handler, HUD hud) {
		this.handler = handler;
		this.hud = hud;
	}
	
	public void render(Graphics g) {
		g.setColor(Color.WHITE);
		g.setFont(title);
		g.drawString("Beam Store", 150, 80);
		
		g.setFont(button);
		g.drawString("+ Health", 150, 155);
		
		g.setFont(price);
		g.drawString("Cost: $"+healthCost, 155, 185);
		
		g.setFont(button);
		g.drawRect(130, 115, 130, 95);
		
		g.setFont(button);
		g.drawString("+ Speed", 375, 155);
		
		g.setFont(price);
		g.drawString("Cost: $"+speedCost, 375, 185);
		
		g.setFont(button);
		g.drawRect(130, 250, 130, 95);
		
		g.setFont(button);
		g.drawString("* Health", 155, 290);
		
		g.setFont(price);
		g.drawString("Cost: $"+refillHealthCost, 155, 320);
		
		g.setFont(button);
		g.drawRect(350, 115, 130, 95);
		
		g.setFont(button);
		g.drawString("Back", 290, 412);
		
		g.setFont(button);
		g.drawRect(250, 380, 130, 50);
	}
	
	public void mousePressed(MouseEvent e) {}
	
	public void mouseReleased(MouseEvent e) {
		int my = e.getY();
		int mx = e.getX();
		
		if(Game.gameState == STATE.Store) {
									//+ health
			if(mouseOver(mx, my, 130, 115, 130, 95)) {
				AudioManager.getSound("click").play();
				if(purchase("add_health")) {
					Game.gameState = STATE.Game;
				}
			}
									//+ speed
			if(mouseOver(mx, my, 130, 250, 130, 95)) {
				AudioManager.getSound("click").play();
				if(purchase("add_speed")) {
					Game.gameState = STATE.Game;
				}
			}
									//refill health
			if(mouseOver(mx, my, 350, 115, 130, 95)) {
				AudioManager.getSound("click").play();
				if(purchase("refill_health")) {
					Game.gameState = STATE.Game;
				}
			}
									//back
			if(mouseOver(mx, my, 250, 380, 130, 50)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Game;
			}
		}
		
	}
	
	private boolean mouseOver(int mx, int my, int x, int y, int width, int height) {
		if(mx > x && mx < x + width) {
			return (my > y && my < y + height);
		} else {
			return false;
		}
	}
	
	private boolean purchase(String name) {
		switch(name) {
			case "add_health":
				if(hud.getScore() >= healthCost) {
					hud.setScore(hud.getScore() - healthCost);
					healthCost += 1000;
					hud.bounds += 20;
					HUD.HEALTH = (100 + (hud.bounds / 2));
					return true;
				} else {
					JOptionPane.showMessageDialog(null, "Your score is not high enough for that! You score: " + hud.getScore(), "Alert!", JOptionPane.ERROR_MESSAGE);
					return false;
				}
			case "refill_health":
				if(hud.getScore() >= refillHealthCost) {
					hud.setScore(hud.getScore() - refillHealthCost);
					refillHealthCost += 100;
					HUD.HEALTH = (100 + (hud.bounds / 2));
					return true;
				} else {
					JOptionPane.showMessageDialog(null, "Your score is not high enough for that! You score: " + hud.getScore(), "Alert!", JOptionPane.ERROR_MESSAGE);
					return false;
				}
			case "add_speed":
				
				System.out.println("Current Money: " + hud.getScore() + " Cost: " + speedCost);
				
				if(hud.getScore() >= speedCost) {
					hud.setScore(hud.getScore() - speedCost);
					speedCost += 2000;
					handler.speed++;
					return true;
				} else {
					JOptionPane.showMessageDialog(null, "Your score is not high enough for that! You score: " + hud.getScore(), "Alert!", JOptionPane.ERROR_MESSAGE);
					return false;
				}
			default:
			break;
		}
		
		return false;
	}
	
}
