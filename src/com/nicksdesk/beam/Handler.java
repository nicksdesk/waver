package com.nicksdesk.beam;

import java.awt.Graphics;
import java.util.LinkedList;

public class Handler {

	public int speed = 5;
	
	LinkedList<GameObject> object = new LinkedList<GameObject>();
	
	public void tick() {
		for(int i = 0; i < object.size(); i++) {
			GameObject temp = object.get(i);
			temp.tick();
		}
	}
	
	public void render(Graphics g) {
		for(int i = 0; i < object.size(); i++) {
			GameObject temp = object.get(i);
			temp.render(g);
		}
	}
	
	public void addObject(GameObject object) {
		this.object.add(object);
	}
	
	public void removeObject(GameObject object) {
		this.object.remove(object);
	}
	
	public void clearBoss() {
		for(int i = 0; i < object.size(); i++) {
			GameObject temp = object.get(i);
			if(temp.getId() == ID.BossEnemy) {
				this.object.remove(temp);
			}
		}
	}
	
	public void clearEnemies() {
		for(int i = 0; i < object.size(); i++) {
			GameObject temp = object.get(i);
			if(temp.getId() == ID.Player) {
				object.clear();
				if(Game.gameState != Game.STATE.End) this.addObject(new Player((int)temp.getX(), (int)temp.getY(), ID.Player, this));
			}
		}
	}
	
}
