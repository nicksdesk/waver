package com.nicksdesk.beam;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

public class HUD {

	public int bounds = 0;
	public static float HEALTH = 100;
	private int greenVal = 255;
	
	private int score = 0;
	private float level = 1;
	
	private Font hudFont = new Font("arial", 1, 12);
	private Font storeFont = new Font("arial", 1, 15);
	
	public void tick() {
		HEALTH = Game.clamp(HEALTH, 0, 100 + (bounds / 2));
		
		greenVal = (int) Game.clamp((int)greenVal, 0, 255);
		greenVal = (int) ((int)HEALTH * 2);
		
		score++;
	}
	
	public void render(Graphics g) {
		g.setColor(Color.GRAY);
		g.fillRect(15, 15, 200 + bounds, 32);
		g.setColor(new Color(75, greenVal, 0));
		g.fillRect(15, 15, (int)HEALTH * 2, 32);
		g.setColor(Color.WHITE);
		g.drawRect(15, 15, 200 + bounds, 32);
		
		g.setFont(storeFont);
		g.drawString("[Shift] = Store", 15, 64);
		g.drawString("[Space] = Pause", 15, 80);
		
		g.setFont(hudFont);
		g.drawString("Score: " + score, 15, 100);
		g.drawString("Level: " + level, 15, 114);
	}
	
	public void setScore(int score) {
		this.score = score;
	}
	
	public int getScore() {
		return score;
	}
	
	public void setLevel(float level) {
		this.level = level;
	}
	
	public float getLevel() {
		return level;
	}
}
