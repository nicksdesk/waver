package com.nicksdesk.beam;

public enum ID {

	Player(),
	Trail(),
	FastEnemy(),
	BasicEnemy(),
	ExtremeEnemy(),
	TrackingEnemy(),
	Particles(),
	BossEnemy()
	
	
}
