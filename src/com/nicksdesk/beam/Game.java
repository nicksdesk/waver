package com.nicksdesk.beam;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.util.Random;

import javax.swing.UIManager;

public class Game extends Canvas implements Runnable {

	private static final long serialVersionUID = -4530498988120082712L;
	public static final String GAME_NAME = "Beam";
	public static final int WIDTH = 640, HEIGHT = WIDTH / 12 * 9;
	
	/*
	 * 0 = normal
	 * 1 = extreme
	 */
	public static int GAME_DIFF = 0;
	public static boolean paused = false;
	
	private Thread thread;
	private boolean running = false;
	private boolean showFPS = false;
	
	private Random r;
	private Handler handler;
	private HUD hud;
	private Spawn spawn;
	private Menu menu;
	private Store store;
	
	public enum STATE {
		Menu,
		Select,
		Store,
		Options,
		Game,
		End
	};
	
	public static STATE gameState = STATE.Menu;
	
	public static void main(String args[]) {
		try {
			OSManager.setNatives();			
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			new Game();
		} catch(Exception e) {
			e.printStackTrace();
		}
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
			@Override
			public void run() {
				AudioManager.cleanUp();
			}
		}, "Cleanup-Thread"));
	}
	
	public Game() {
		handler = new Handler();
		hud = new HUD();
		menu = new Menu(this, handler, hud);
		store = new Store(handler, hud);
		
		this.addKeyListener(new KeyInput(handler));
		
		this.addMouseListener(menu);
		this.addMouseListener(store);
		
		try {
			AudioManager.init();
			//AudioManager.getMusic("background").loop();
		} catch(Exception e) {
			e.printStackTrace();
		}
		
		new Window(WIDTH, HEIGHT, Game.GAME_NAME, this);

		spawn = new Spawn(handler, hud);
		r = new Random();
		
		if(gameState == STATE.Game) {
			handler.addObject(new Player(WIDTH/2-32, HEIGHT/2-32, ID.Player, handler));
			handler.addObject(new BasicEnemy(r.nextInt(WIDTH), r.nextInt(HEIGHT), ID.BasicEnemy, handler));
		} else {
			for(int i = 0; i < 10; i++) {
				handler.addObject(new Particles(r.nextInt(WIDTH - 1), r.nextInt(HEIGHT - 1), ID.Particles, handler));
			}
		}
	}
	
	public void run() {
		this.requestFocus();
		long lastTime = System.nanoTime();
		double delta = 0;
		double amountOfTicks = 60.0;
		double ns = 1000000000 / amountOfTicks;
		long timer = System.currentTimeMillis();
		int frames = 0;
		while(running) {
			long now = System.nanoTime();
			delta += (now - lastTime) / ns;
			lastTime = now;
			while(delta >= 1) {
				tick();
				delta--;
			}
			if(running)
				render();
			frames++;
			
			if(System.currentTimeMillis() - timer > 1000) {
				timer += 100;
				if(showFPS) {
					System.out.println("FPS: " + frames);
				}
				frames = 0;
			}
		}
		this.stop();
	}
	
	private void tick() {
		if(gameState == STATE.Game) {
			if(!paused) {
				hud.tick();
				spawn.tick();
				handler.tick();
				if(HUD.HEALTH <= 0) {
					HUD.HEALTH = 100;
					gameState = STATE.End;
					handler.clearEnemies();
					for(int i = 0; i < 10; i++) {
						handler.addObject(new Particles(r.nextInt(WIDTH - 1), r.nextInt(HEIGHT - 1), ID.Particles, handler));
					}
				}
			}
		} else if(gameState == STATE.Menu || gameState == STATE.End || gameState == STATE.Select) {
			menu.tick();
			handler.tick();
		}
	}
	
	private void render() {
		BufferStrategy buffs = this.getBufferStrategy();
		if(buffs == null) {
			this.createBufferStrategy(3);
			return;
		}
		Graphics g = buffs.getDrawGraphics();
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, WIDTH, HEIGHT);
		
		if(paused) {
			Font pauseFont = new Font("arial", 1, 60);
			g.setColor(Color.WHITE);
			g.setFont(pauseFont);
			g.drawString("Game Paused", 125, 200);
		}
		
		
		
		if(gameState == STATE.Game) {
			handler.render(g);
			hud.render(g);
		} else if(gameState == STATE.Store) {
			store.render(g);
		} else if(gameState == STATE.Menu || gameState == STATE.Options || gameState == STATE.End || gameState == STATE.Select) {
			handler.render(g);
			menu.render(g);
		}
		
		g.dispose();
		buffs.show();
	}
	
	public static float clamp(float var, float min, float max) {
		if(var >= max) 
			return var = max;
		else if(var <= min)
			return var = min;
		return var;
	}
	
	public synchronized void start() {
		thread = new Thread(this);
		thread.start();
		running = true;
	}
	
	public synchronized void stop() {
		try {
			thread.join();
			running = false;
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
