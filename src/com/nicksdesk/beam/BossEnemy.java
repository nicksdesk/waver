package com.nicksdesk.beam;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.util.Random;

public class BossEnemy extends GameObject {

	private Handler handler;
	private Random r = new Random();
	
	private int moveTimer = 80;
	private int shootTimer = 50;
	
	public BossEnemy(float x, float y, ID id, Handler handler) {
		super(x, y, id);
		this.handler = handler;
		velX = 0;
		velY = 2;
	}
	
	public Rectangle getBounds() {
		return new Rectangle((int)x, (int)y, 64, 64);
	}

	public void tick() {
		x += velX;
		y += velY;
		
		if(moveTimer <= 0) velY = 0;
		else moveTimer --;
		
		if(moveTimer <= 0) shootTimer--;
		
		if(shootTimer <= 0) {
			if(velX == 0) velX = 2;
			velX += 0.001f;
			int spawner = r.nextInt(10);
			if(spawner == 0) handler.addObject(new BossBulletEnemy((int)x + 48, (int)y + 48, ID.BasicEnemy, handler));
		}

		if(x <= 0 || x >= Game.WIDTH - 96) velX *= -1;
		
		//handler.addObject(new Trail((int)x, (int)y, Color.RED, 64, 64, 0.008f, handler, ID.Trail));
		
	}

	public void render(Graphics g) {
		g.setColor(Color.RED);
		g.fillRect((int)x, (int)y, 64, 64);
	}
}