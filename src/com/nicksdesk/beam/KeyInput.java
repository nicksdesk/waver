package com.nicksdesk.beam;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;

import com.nicksdesk.beam.Game.STATE;

public class KeyInput extends KeyAdapter {

	private boolean[] keyDown = new boolean[4];
	private Handler handler;
	
	public KeyInput(Handler handler) {
		for(int i = 0; i < keyDown.length; i++) {
			//0 = w, 1 = s, 2 = d, 3 = a
			keyDown[i] = false;
		}
		this.handler = handler;
	}
	
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject temp = handler.object.get(i);
			if(temp.getId() == ID.Player) {
				if(key == KeyEvent.VK_W) {
					temp.setVelY(-handler.speed);
					keyDown[0] = true;
				}
				if(key == KeyEvent.VK_S) {
					temp.setVelY(handler.speed);
					keyDown[1] = true;
				}
				if(key == KeyEvent.VK_D) {
					temp.setVelX(handler.speed);
					keyDown[2] = true;
				}
				if(key == KeyEvent.VK_A) {
					temp.setVelX(-handler.speed);
					keyDown[3] = true;
				}
			}
		}
		if(key == KeyEvent.VK_SPACE) {
			if(Game.gameState == Game.STATE.Game) {
				Game.paused = !Game.paused;
			}
		}
		if(key == KeyEvent.VK_ESCAPE) {
			Object[] options = {
				"Main Menu", "Quit", "Cancel"
			};
			int action = JOptionPane.showOptionDialog(null, "Would you like to exit or go back?", "Quit", JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.QUESTION_MESSAGE, null, options, options[2]);
			if(action == JOptionPane.YES_OPTION) {
				//put player back to menu
			} else if(action == JOptionPane.NO_OPTION) {
				System.exit(0);
			} else if(action == JOptionPane.CANCEL_OPTION) {
				return;
			}
		}
		if(key == KeyEvent.VK_SHIFT) {
			if(Game.gameState == STATE.Game) {
				Game.gameState = STATE.Store;
			} else if(Game.gameState == STATE.Store) {
				Game.gameState = STATE.Game;
			}
		}
	}
	
	public void keyReleased(KeyEvent e) {
		int key = e.getKeyCode();
		for(int i = 0; i < handler.object.size(); i++) {
			GameObject temp = handler.object.get(i);
			if(temp.getId() == ID.Player) {
				if(key == KeyEvent.VK_W) keyDown[0] = false;//temp.setVelY(0);
				if(key == KeyEvent.VK_S) keyDown[1] = false;//temp.setVelY(0);
				if(key == KeyEvent.VK_D) keyDown[2] = false;//temp.setVelX(0);
				if(key == KeyEvent.VK_A) keyDown[3] = false;//temp.setVelX(0);
				
				if(!keyDown[0] && !keyDown[1]) temp.setVelY(0);
				if(!keyDown[2] && !keyDown[3]) temp.setVelX(0);
			}
		}
	}
	
}
