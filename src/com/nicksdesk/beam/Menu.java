package com.nicksdesk.beam;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Random;

import com.nicksdesk.beam.Game.STATE;

public class Menu extends MouseAdapter {

	//TODO: stop being lazy...
	@SuppressWarnings("unused")
	private Game game;
	
	private Handler handler;
	private HUD hud;
	private Random r = new Random();
	
	public Menu(Game game, Handler handler, HUD hud) {
		this.game = game;
		this.handler = handler;
		this.hud = hud;
	}
	
	public void mousePressed(MouseEvent e) {}
	
	public void mouseReleased(MouseEvent e) {
		
		int mx = e.getX();
		int my = e.getY();
		
		if(Game.gameState == STATE.Menu) {
										//play button
			if(this.mouseOver(mx, my, 220, 150, 200, 64)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Select;
			}
										//quit button
			if(this.mouseOver(mx, my, 220, 350, 200, 64)) {
				AudioManager.getSound("click").play();
				try {
					Thread.sleep(1000);
					System.exit(0);
				} catch(Exception err) {
					err.printStackTrace();
				}
			}
										//options button
			if(this.mouseOver(mx, my, 220, 250, 200, 64)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Options;
			}
		} else if(Game.gameState == STATE.Options) {
			
										//back button
			if(this.mouseOver(mx, my, 220, 350, 200, 64)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Menu;
			}
		} else if(Game.gameState == STATE.End) {
										//back button
			if(this.mouseOver(mx, my, 220, 350, 200, 64)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Menu;
			}
		} else if(Game.gameState == STATE.Select) {
										//normal mode
			if(this.mouseOver(mx, my, 220, 150, 200, 64)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Game;
				Game.GAME_DIFF = 0;
				handler.addObject(new Player(Game.WIDTH/2-32, Game.HEIGHT/2-32, ID.Player, handler));
				handler.clearEnemies();
				handler.addObject(new BasicEnemy(r.nextInt(Game.WIDTH), r.nextInt(Game.HEIGHT), ID.BasicEnemy, handler));
			}
										//extreme mode
			if(this.mouseOver(mx, my, 220, 250, 200, 64)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Game;
				Game.GAME_DIFF = 1;
				handler.addObject(new Player(Game.WIDTH/2-32, Game.HEIGHT/2-32, ID.Player, handler));
				handler.clearEnemies();
				handler.addObject(new ExtremeEnemy(r.nextInt(Game.WIDTH), r.nextInt(Game.HEIGHT), ID.BasicEnemy, handler));
			}
										//back button
			if(this.mouseOver(mx, my, 220, 350, 200, 64)) {
				AudioManager.getSound("click").play();
				Game.gameState = STATE.Menu;
			}
		}
	}
	
	private boolean mouseOver(int mx, int my, int x, int y, int width, int height) {
		if(mx > x && mx < x + width) {
			return (my > y && my < y + height);
		} else {
			return false;
		}
	}
	
	public void tick() {}
	
	public void render(Graphics g) {
		
		Font fontTitle = new Font("arial", 1, 80);
		Font btnFont = new Font("arial", 1, 40);
		Font alertFont = new Font("arial", 1, 15);
		
		if(Game.gameState == STATE.Menu) {
			g.setFont(fontTitle);
			g.setColor(Color.WHITE);
			g.drawString(Game.GAME_NAME, 210, 100);
			
			g.setColor(Color.WHITE);
			g.drawRect(220, 150, 200, 64);
			g.setFont(btnFont);
			g.drawString("Play", 275, 195);
			
			g.setColor(Color.WHITE);
			g.drawRect(220, 250, 200, 64);
			g.setFont(btnFont);
			g.drawString("Options", 250, 300);
			
			g.setColor(Color.WHITE);
			g.drawRect(220, 350, 200, 64);
			g.setFont(btnFont);
			g.drawString("Quit", 275, 400);
		} else if(Game.gameState == STATE.Options) {
			g.setFont(fontTitle);
			g.setColor(Color.WHITE);
			g.drawString("Options", 165, 100);
			
			g.setColor(Color.WHITE);
			g.drawRect(220, 350, 200, 64);
			g.setFont(btnFont);
			g.drawString("Back", 275, 400);
		} else if(Game.gameState == STATE.End) {
			g.setFont(fontTitle);
			g.setColor(Color.WHITE);
			g.drawString("Beam", 200, 100);
			
			g.setColor(Color.WHITE);
			g.setFont(alertFont);
			g.drawString("Game over! You finished with a score of: " + hud.getScore(), 150, 250);
		
			g.drawRect(220, 350, 200, 64);
			g.setFont(btnFont);
			g.drawString("Back", 275, 400);
		} else if(Game.gameState == STATE.Select) {
			g.setFont(fontTitle);
			g.setColor(Color.WHITE);
			g.drawString("Difficulty", 150, 100);
			
			g.setColor(Color.WHITE);
			g.drawRect(220, 150, 200, 64);
			g.setFont(btnFont);
			g.drawString("Normal", 255, 195);
			
			g.setColor(Color.WHITE);
			g.drawRect(220, 250, 200, 64);
			g.setFont(btnFont);
			g.drawString("Extreme", 245, 300);
			
			g.setColor(Color.WHITE);
			g.drawRect(220, 350, 200, 64);
			g.setFont(btnFont);
			g.drawString("Back", 275, 400);
		}
		
	}
	
}
