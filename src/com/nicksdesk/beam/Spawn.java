package com.nicksdesk.beam;

import java.util.Random;

public class Spawn {

	private Handler handler;
	private HUD hud;
	private Random r = new Random();
	
	private int scoreKeep = 0;
	
	public Spawn(Handler handler, HUD hud) {
		this.handler = handler;
		this.hud = hud;
	}
	
	public void tick() {
		scoreKeep++;
		if(scoreKeep >= 500) {
			scoreKeep = 0;
			hud.setLevel(hud.getLevel() + 1);
			
			switch(Game.GAME_DIFF) {
				case 0:
					if((int)hud.getLevel() != 0) {
						handler.addObject(new BasicEnemy(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.BasicEnemy, handler));
						if(((int)hud.getLevel() % 2) == 0) {
							handler.addObject(new FastEnemy(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.FastEnemy, handler));
						}
						if(((int)hud.getLevel() % 3) == 0) {
							handler.addObject(new TrackingEnemy(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.TrackingEnemy, handler));
						}
						if(((int)hud.getLevel() % 10) == 0) {
							handler.clearEnemies();
							handler.addObject(new BossEnemy((Game.WIDTH / 2) - 48, -125, ID.BossEnemy, handler));
						} else {
							handler.clearBoss();
						}
					}
				break;
				case 1:
					if((int)hud.getLevel() != 0) {
						handler.addObject(new ExtremeEnemy(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.BasicEnemy, handler));
						if(((int)hud.getLevel() % 2) == 0) {
							handler.addObject(new FastEnemy(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.FastEnemy, handler));
						}
						if(((int)hud.getLevel() % 3) == 0) {
							handler.addObject(new TrackingEnemy(r.nextInt(Game.WIDTH - 50), r.nextInt(Game.HEIGHT - 50), ID.TrackingEnemy, handler));
						}
						if(((int)hud.getLevel() % 10) == 0) {
							handler.clearEnemies();
							handler.addObject(new BossEnemy((Game.WIDTH / 2) - 48, -125, ID.BossEnemy, handler));
						} else {
							handler.clearBoss();
						}
					}
				break;
				default:
				break;
			}
			
			
			
			
		}
	}	
}
